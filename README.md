# Modeling Patterning and Scaling with Applications in Tissue Engineering

The code found in this repository analyses a two species reaction diffusion system.

The file:
[patterning.ipynb](patterning.ipynb)
Contains a jupyter notebook with the calculations of patterning. To follow the work done please open this file.

[species.py](species.py) contains classes used to abstract away some of the details of the caluclation and keep the Notebook looking cleaner.