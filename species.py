import numpy as np

class ReacSpecies:
    """
    Simple class to assign reaction properties to a chemical species
    """

    def __init__(self, alpha, beta, diff, init_conc=0.0):
        self.alpha = alpha
        self.beta = beta
        self.diff = diff
        self.conc = []
        self.initialConc = init_conc

        self.lam = np.sqrt(self.diff/self.beta)


class System:
    """
    Stores the parameters of the reaction-diffusion system
    """
    
    steady = False
    steady_count = 0

    def __init__(self, length_x, length_t, A, B, delta_x = 0.1, delta_t = 0.001):
        self.delta_t = delta_t
        self.delta_x = delta_x
        
        self.speciesA = A
        self.speciesB = B

        self.length_x = length_x
        self.length_t = length_t
        self.check_time_step()
        self.num_time_points = int((length_t - 0) / self.delta_t)
        self.num_x_points = int((length_x - 0) / self.delta_x)

        self.x = np.linspace(-self.delta_x, length_x+self.delta_x, self.num_x_points+2)
        self.t = np.linspace(0, self.length_t, self.num_time_points)

    def check_time_step(self):
        if self.delta_t < 2 * self.delta_x ** 2 / (4 * self.speciesA.diff + self.speciesA.beta * self.delta_x ** 2) and \
                self.delta_t < 2 * self.delta_x ** 2 / (4 * self.speciesB.diff + self.speciesB.beta * self.delta_x ** 2):
            
            print("delta t :  ", self.delta_t)

        else:
            self.delta_t = 0.8 * min(2 * self.delta_x ** 2 / (4 * self.speciesA.diff + self.speciesA.beta * self.delta_x ** 2),
                                    2 * self.delta_x ** 2 / (4 * self.speciesB.diff + self.speciesB.beta * self.delta_x ** 2))

            print("delta t :  ", self.delta_t)
        
        
    def steady_state(self, timePt):
        """
        Method to check if the system has reached a steady-state

        :param timePt: the point in time the system is at
        """
        flag = False
        index = 1000
        if timePt > index:
            if np.abs((self.speciesA.maxConc[timePt-1] - self.speciesA.maxConc[timePt-1-index])) < 0.001:
                flag = True
        
        return flag